package com.fiap.nav.mb;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.apache.axis2.AxisFault;

import com.fiap.nac.ws.EstoqueBOStub;
import com.fiap.nac.ws.EstoqueBOStub.ConsultarProduto;
import com.fiap.nac.ws.EstoqueBOStub.ConsultarProdutoResponse;
import com.fiap.nac.ws.EstoqueBOStub.ProdutoTO;

@ManagedBean
@RequestScoped
public class EstoqueBean {

	private ProdutoTO prod;

	private int codProduto;
	
	public void consultaProduto(){
		
		try {
			
			EstoqueBOStub stub = new EstoqueBOStub();
			
			ConsultarProduto request = new ConsultarProduto();
			
			request.setCodProduto(codProduto);
			
			ConsultarProdutoResponse response = new ConsultarProdutoResponse();
			
			setProd(response.get_return());
			
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public ProdutoTO getProd() {
		return prod;
	}

	public void setProd(ProdutoTO prod) {
		this.prod = prod;
	}
	
	
	
}
